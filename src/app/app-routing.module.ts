import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {NgModule} from '@angular/core';
import {NotFoundComponent} from './not-found/not-found.component';

const ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about', loadChildren: './about/about.module#AboutModule'}, /*loadChildren carrega o módulo Lazy*/
  {path: 'restaurants/order', loadChildren: './restaurants/order/order.module#OrderModule'},
  {path: 'restaurants/order-summary', loadChildren: './restaurants/order-summary/order-summary.module#OrderSummaryModule'},
  {path: 'restaurants', loadChildren: './restaurants/restaurants.module#RestaurantsModule'},
  {path: '**', component: NotFoundComponent}
];

/**
 * preloadingStrategy: PreloadAllModules = Faz com que os módulos Lazy sejam pré carregados pem background, ficando disponível para o momento de uso
 */
@NgModule({
  imports: [ RouterModule.forRoot(ROUTES, { enableTracing: true, preloadingStrategy: PreloadAllModules }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
