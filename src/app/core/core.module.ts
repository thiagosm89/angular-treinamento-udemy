import {NgModule} from '@angular/core';
import {ShoppingCartService} from '../restaurant-detail/shopping-cart/shopping-cart.service';
import {RestaurantsService} from '../restaurants/restaurants.service';
import {OrderService} from '../restaurants/order/order.service';

/*
  Criamos o CoreModule, pois precisamos compartilhar alguns serviços entre todos os Módulos, inclusive os Lazy
 */
@NgModule({
  providers: [ShoppingCartService, RestaurantsService, OrderService]
})
export class CoreModule {}
