import {NgModule, OnInit} from '@angular/core';
import {AboutComponent} from './about.component';
import {AboutRoutingModule} from './about.routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    AboutRoutingModule
  ],
  declarations: [AboutComponent]
})
export class AboutModule { }
