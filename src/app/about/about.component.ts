import {AfterContentInit, Component, OnInit} from '@angular/core';

@Component({
  selector: 'mt-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit, AfterContentInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit');
  }

}
