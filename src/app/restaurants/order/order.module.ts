import {NgModule} from '@angular/core';
import {OrderComponent} from './order.component';
import {OrderItemsComponent} from './order-items/order-items.component';
import {DeliveryCostsComponent} from './delivery-costs/delivery-costs.component';
import {SharedModule} from '../../shared/shared.module';
import {OrderRoutingModule} from './order-routing.module';

@NgModule({
  declarations: [OrderComponent, OrderItemsComponent, DeliveryCostsComponent],
  imports: [SharedModule, OrderRoutingModule]
})
export class OrderModule {}
