import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OrderComponent} from './order.component';

const ROUTES: Routes = [
    {path: '', component: OrderComponent}
  ]
;

@NgModule({
  imports: [ RouterModule.forChild(ROUTES) ],
  exports: [ RouterModule ]
})
export class OrderRoutingModule { }
