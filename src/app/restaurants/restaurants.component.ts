import {Component, OnInit} from '@angular/core';
import {Restaurant} from './restaurant/restaurant.model';
import {RestaurantsService} from './restaurants.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  animations: [
    trigger('toggleSearch', [
      state( 'hidden', style({
        opacity: 0,
        'max-height': '0px'
      }) ),
      state( 'visible', style({
        opacity: 1,
        'max-height': '70px',
        'margin-top': '20px'
      }) ),
      transition( '* => *', [
          animate('250ms 0s ease-in-out')
        ]
      )
    ])
  ]
})
export class RestaurantsComponent implements OnInit {

  restaurants: Array<Restaurant>;
  searchBarState = 'hidden';

  searchForm: FormGroup;
  searchControl: FormControl;

  constructor( private restaurantService: RestaurantsService,
               private fb: FormBuilder) { }

  ngOnInit() {

    this.searchControl = this.fb.control('');

    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    });

    this.searchControl.valueChanges
      .debounceTime( 500 ) /*Depois de 500 mls que ele vai executar os próximos operadores*/
      .distinctUntilChanged() /*O novo termo digitado não pode ser igual ao o último que estava no campo, se forem iguais, não é buscado novamente*/
      .do( searchTerm => console.log( searchTerm ) ) /*não é necessário, mas vou deixar pois é legal para questão de exemplo*/
      .switchMap(
        searchTerm => this.restaurantService.restaurants( searchTerm )
          .catch( error => Observable.from([]) ) /*Caso saia do ar o servidor backend, então o erro será tratado e não irá parar de funcionar o componente de busca pois o erro não será propagado para o observable de valueChange*/
      )
      .subscribe( restaurants => this.setRestaurants(restaurants) )
    ;

    this.restaurantService.restaurants().subscribe( restaurants => this.setRestaurants(restaurants) );
  }

  private setRestaurants(restaurants: Array<Restaurant>): void {
    this.restaurants = restaurants;
  }

  toggleSearch() {
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible' : 'hidden';
  }
}
