import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RestaurantsComponent} from './restaurants.component';
import {RestaurantDetailComponent} from '../restaurant-detail/restaurant-detail.component';
import {MenuComponent} from '../restaurant-detail/menu/menu.component';
import {ReviewsComponent} from '../restaurant-detail/reviews/reviews.component';

const ROUTES: Routes = [
    {path: '', component: RestaurantsComponent},
    {
      path: ':id', component: RestaurantDetailComponent, pathMatch: 'full',
      children: [
        {path: '', component: MenuComponent, pathMatch: 'full'},
        {path: 'menu', component: MenuComponent},
        {path: 'reviews', component: ReviewsComponent}
      ]
    }
  ]
;

@NgModule({
  imports: [ RouterModule.forChild(ROUTES) ],
  exports: [ RouterModule ]
})
export class RestaurantsRoutingModule { }
