import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {OrderSummaryComponent} from './order-summary.component';

const ROUTES: Routes = [
  {path: '', component: OrderSummaryComponent}
];

@NgModule({
  imports: [ RouterModule.forChild(ROUTES) ],
  exports: [ RouterModule ]
})
export class OrderSummaryRoutingModule {}
