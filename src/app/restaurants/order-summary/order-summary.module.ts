import { NgModule } from '@angular/core';
import {OrderSummaryComponent} from './order-summary.component';
import {SharedModule} from '../../shared/shared.module';
import {OrderSummaryRoutingModule} from './order-summary-routing.module';

@NgModule({
  imports: [
    SharedModule,
    OrderSummaryRoutingModule
  ],
  declarations: [
    OrderSummaryComponent
  ]
})
export class OrderSummaryModule { }
