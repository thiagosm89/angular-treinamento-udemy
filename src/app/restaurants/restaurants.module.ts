import { NgModule } from '@angular/core';
import {RestaurantsComponent} from './restaurants.component';
import {RestaurantsRoutingModule} from './restaurants-routing.module';
import {RestaurantComponent} from './restaurant/restaurant.component';
import {RestaurantDetailComponent} from '../restaurant-detail/restaurant-detail.component';
import {MenuComponent} from '../restaurant-detail/menu/menu.component';
import {MenuItemComponent} from '../restaurant-detail/menu-item/menu-item.component';
import {ShoppingCartComponent} from '../restaurant-detail/shopping-cart/shopping-cart.component';
import {ReviewsComponent} from '../restaurant-detail/reviews/reviews.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RestaurantsRoutingModule
  ],
  declarations: [
    RestaurantsComponent,
    RestaurantComponent,
    RestaurantDetailComponent,
    MenuComponent,
    MenuItemComponent,
    ShoppingCartComponent,
    ReviewsComponent
  ]
})
export class RestaurantsModule { }
